++++++++++[
   >+++++++>++++++++++>+++>+<<<<-
] A loop that reduces the amount of needed actions by a large margin
>++. print 'H'
>+. print 'e'
+++++++. 'l'
. 'l'
+++. 'o'
>++. space
<<+++++++++++++++. 'W'
>. 'o'
+++. 'r'
------. 'l'
--------. 'd'
>+. '!'
>. newline

The same program without comments:
++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.
